package com.example.pbawebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PbaWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PbaWebServiceApplication.class, args);
	}

}
